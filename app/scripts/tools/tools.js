'use strict';

/**
 * Created by Adam on 30/04/2017.
 */
function myLoop(x) {
  var i, y, xLen, txt;
  txt = "";
  x = x.childNodes;
  xLen = x.length;
  for (i = 0; i < xLen; i++) {
    y = x[i];
    if (y.nodeType !== 3) {
      if (y.childNodes[0] !== undefined) {
        txt += myLoop(y);
      }
    } else {
      txt += y.parentElement.tagName + ' : ' + y.nodeValue + '<br>';
    }
  }
  return txt;
}
function xmlDrctDbtTxInf1(xml) {
  var txt = '<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>' +
    '<ns2:DrctDbtTxInf xmlns:ns2=\"http:\/\/univ.fr/sepa\">' +
    '<ns2:PmtId>' + xml.pmtId + '</ns2:PmtId>' +
    '<ns2:InstdAmt Ccy=\"' + xml.ccy + '\">' + xml.instdAmt + '</ns2:InstdAmt>' +
    '<ns2:DrctDbtTx>' +
    '<ns2:MndtRltdInf>' +
    '<ns2:MndtId>' + xml.id + '</ns2:MndtId>' +
    '<ns2:DtOfSgntr>2017-04-03</ns2:DtOfSgntr>' +
    '</ns2:MndtRltdInf>' +
    '</ns2:DrctDbtTx>' +
    '<ns2:DbtrAgt>' +
    '<ns2:FinInstnId>' +
    '<Othr>' +
    '<Id>' + xml.id + '</Id>' +
    '</Othr>' +
    '<ns2:BIC>' + xml.bic + '</ns2:BIC>' +
    '</ns2:FinInstnId>' +
    '</ns2:DbtrAgt>' +
    '<ns2:Dbtr>' +
    '<ns2:Nm>' + xml.nm + '</ns2:Nm>' +
    '</ns2:Dbtr>' +
    '<ns2:DbtrAcct>' +
    '<Id>' +
    '<ns2:IBAN>' + xml.iban + '</ns2:IBAN>' +
    '<ns2:PrvtId>' +
    '<Othr>' +
    '<Id>' + xml.othrTypeId + '</Id>' +
    '<ns2:SchmeNm>' +
    '<ns2:Prtry>' + xml.prtry + '</ns2:Prtry>' +
    '</ns2:SchmeNm>' +
    '</Othr>' +
    '</ns2:PrvtId>' +
    '</Id>' +
    '</ns2:DbtrAcct>';
  txt += '<ns2:RmtInf>' + xml.rmtInf[0].text + '</ns2:RmtInf>';
  txt += '</ns2:DrctDbtTxInf>';
  return txt;
}
function xmlDrctDbtTxInf(xml) {
  var txt = '<?xml version="1.0"?>' +
    '<ns2:DrctDbtTxInf xmlns:ns2="http://univ.fr/sepa">' +
    '<ns2:PmtId>' + xml.pmtId + '</ns2:PmtId>' +
    '<ns2:InstdAmt Ccy="' + xml.ccy + '">' + xml.instdAmt + '</ns2:InstdAmt>' +
    '<ns2:DrctDbtTx>' +
    '<ns2:MndtRltdInf>' +
    '<ns2:MndtId>' + xml.mndtId + '</ns2:MndtId>' +
    '<ns2:DtOfSgntr>' + xml.mydate + '</ns2:DtOfSgntr>' +
    '</ns2:MndtRltdInf>' +
    '</ns2:DrctDbtTx>' +
    '<ns2:DbtrAgt>' +
    '<ns2:FinInstnId>' +
    '<Othr>' +
    '<Id>' + xml.id + '</Id>' +
    '</Othr>' +
    '</ns2:FinInstnId>' +
    '</ns2:DbtrAgt>' +
    '<ns2:Dbtr>' +
    '<ns2:Nm>' + xml.nm + '</ns2:Nm>' +
    '</ns2:Dbtr>' +
    '<ns2:DbtrAcct>' +
    '<Id>' +
    '<ns2:IBAN>' + xml.iban + '</ns2:IBAN>' +
    '</Id>' +
    '</ns2:DbtrAcct>';
  for (var i = 0; i < xml.rmtInf.length; i++) {
    txt += '<ns2:RmtInf>' + xml.rmtInf[i].text + '</ns2:RmtInf>';
  }
  txt += '</ns2:DrctDbtTxInf>';
  return txt;
}

