'use strict';

/**
 * @ngdoc function
 * @name web2CliApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the web2CliApp
 */

//var hamid="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns2:DrctDbtTxInf xmlns:ns2=\"http:\/\/univ.fr/sepa\"><ns2:PmtId>pmtIdExampleMEd</ns2:PmtId><ns2:InstdAmt Ccy=\"CcyExample\">10.00</ns2:InstdAmt><ns2:DrctDbtTx><ns2:MndtRltdInf><ns2:MndtId>MndtIdExmple</ns2:MndtId></ns2:MndtRltdInf></ns2:DrctDbtTx><ns2:DbtrAgt><ns2:FinInstnId><Othr><Id>OthrType1Example</Id></Othr><ns2:BIC>BICExample</ns2:BIC></ns2:FinInstnId></ns2:DbtrAgt><ns2:Dbtr><ns2:Nm>NmExmple</ns2:Nm></ns2:Dbtr><ns2:DbtrAcct><Id><ns2:IBAN>IBANExample</ns2:IBAN><ns2:PrvtId><Othr><Id>othrTypeExampleID</Id><ns2:SchmeNm><ns2:Prtry>PrtryExample</ns2:Prtry></ns2:SchmeNm></Othr></ns2:PrvtId></Id></ns2:DbtrAcct><ns2:RmtInf>girafe</ns2:RmtInf><ns2:RmtInf>chameau</ns2:RmtInf><ns2:RmtInf>chat</ns2:RmtInf><ns2:RmtInf>poisson</ns2:RmtInf><ns2:RmtInf>cachalot</ns2:RmtInf></ns2:DrctDbtTxInf>"
angular.module('web2CliApp')
  .filter("trust", ['$sce', function ($sce) {
    return function (htmlCode) {
      return $sce.trustAsHtml(htmlCode);
    };
  }]);
