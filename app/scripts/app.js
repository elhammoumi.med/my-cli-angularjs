'use strict';

/**
 * @ngdoc overview
 * @name web2CliApp
 * @description
 * # web2CliApp
 *
 * Main module of the application.
 */
angular
  .module('web2CliApp', [
    'ngRoute', 'ngTagsInput'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/addtx', {
        templateUrl: 'views/form.html',
        controller: 'FormCtrl'
      })
      .when('/tx/:n', {
        templateUrl: 'views/transaction.html',
        controller: 'TransactionCtrl',
        controllerAs: 'transaction'
      })
      .when('/stats', {
        templateUrl: 'views/stats.html',
        controller: 'StatsCtrl',
        controllerAs: 'stats'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
