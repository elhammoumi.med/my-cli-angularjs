'use strict';

/**
 * @ngdoc service
 * @name web2CliApp.serviceAjax
 * @description
 * # serviceAjax
 * Factory in the web2CliApp.
 */
angular.module('web2CliApp')
  .factory('serviceAjax', function serviceAjax($http) {
    return {
      resume: function () {
        return $http.get('https://test-my-webapp.herokuapp.com/resume');
      },
      stats: function () {
        return $http.get('https://test-my-webapp.herokuapp.com/stats');
      },
      deop: function (details) {
        return $http({
          method: 'POST',
          headers: {'Content-Type': 'application/xml'},
          url: 'https://test-my-webapp.herokuapp.com/depo',
          transformResponse: function (data) {
            return data;
          },
          data: details
        });
      },
      home: function () {
        return $http({
          method: 'GET',
          url: 'https://test-my-webapp.herokuapp.com/',
          transformResponse: function (data) {
            return data;
          }
        });
      },
      transaction: function (n) {
        return $http.get('https://test-my-webapp.herokuapp.com/trx/' + n);
      }
    };
  });
