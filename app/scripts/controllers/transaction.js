'use strict';

/**
 * @ngdoc function
 * @name web2CliApp.controller:TransactionCtrl
 * @description
 * # TransactionCtrl
 * Controller of the web2CliApp
 */
angular.module('web2CliApp')
  .controller('TransactionCtrl', function ($scope, $routeParams, serviceAjax) {
    var loadData = function () {
      serviceAjax.transaction($routeParams.n).then(function (data) {
        var parser, xmlDoc;
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(data.data,'text/xml');
        $scope.tx = myLoop(xmlDoc.documentElement);
      });
    };
    loadData();
  });


