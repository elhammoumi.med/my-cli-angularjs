'use strict';

/**
 * @ngdoc function
 * @name web2CliApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the web2CliApp
 */


angular.module('web2CliApp')
  .controller('AboutCtrl', function ($scope, serviceAjax) {
    var loadData = function () {
      serviceAjax.home().then(function (data) {
        //var result = xmlToJSON.parseString(data.data).listResume[0];
        //$scope.test = JSON.stringify(result.listResume[0].resumes[0]['id'][0]['_text']);
        $scope.homedata = data.data;
      });
    };
    loadData();
  });

