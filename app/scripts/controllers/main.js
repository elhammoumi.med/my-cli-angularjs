'use strict';

/**
 * @ngdoc function
 * @name web2CliApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the web2CliApp
 */
angular.module('web2CliApp')
  .controller('MainCtrl', function ($scope, serviceAjax) {
    var loadData = function () {
      serviceAjax.resume().then(function (data) {
        var result = xmlToJSON.parseString(data.data).listResume[0];
        //$scope.test = JSON.stringify(result.listResume[0].resumes[0]['id'][0]['_text']);
        $scope.items = result.resumes;

      });
    };
    loadData();
  });
