'use strict';

/**
 * @ngdoc function
 * @name web2CliApp.controller:StatsCtrl
 * @description
 * # StatsCtrl
 * Controller of the web2CliApp
 */
angular.module('web2CliApp')
  .controller('StatsCtrl', function ($scope, serviceAjax) {
    var loadData = function () {
      serviceAjax.stats().then(function (data) {
        var parser, xmlDoc;
        parser = new DOMParser();
        xmlDoc = parser.parseFromString(data.data, 'text/xml');
        $scope.statsData = myLoop(xmlDoc.documentElement);
      });
    };
    loadData();
  });
