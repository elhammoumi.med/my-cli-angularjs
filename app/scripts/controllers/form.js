'use strict';

/**
 * @ngdoc function
 * @name web2CliApp.controller:FormCtrl
 * @description
 * # FormCtrl
 * Controller of the web2CliApp
 */

angular.module('web2CliApp')
  .controller('FormCtrl', function ($scope, $window, serviceAjax) {
    $scope.test = '';
    $scope.addtx = function () {
      var details = xmlDrctDbtTxInf($scope.xml);
      //var details = '<?xml version="1.0"?><ns2:DrctDbtTxInf xmlns:ns2="http://univ.fr/sepa"><ns2:PmtId>REF OPE EREEe</ns2:PmtId><ns2:InstdAmt Ccy="">0.00</ns2:InstdAmt><ns2:DrctDbtTx><ns2:MndtRltdInf><ns2:MndtId>MANDAT NO 55555</ns2:MndtId><ns2:DtOfSgntr>2009-09-01</ns2:DtOfSgntr></ns2:MndtRltdInf></ns2:DrctDbtTx><ns2:DbtrAgt><ns2:FinInstnId><Othr><Id>NOTPROVIDED</Id></Othr></ns2:FinInstnId></ns2:DbtrAgt><ns2:Dbtr><ns2:Nm>Mr Debiteur N1</ns2:Nm></ns2:Dbtr><ns2:DbtrAcct><Id><ns2:IBAN>GB29NWBK60161331926819</ns2:IBAN></Id></ns2:DbtrAcct><ns2:RmtInf>Facture N1</ns2:RmtInf></ns2:DrctDbtTxInf>'
      serviceAjax.deop(details).then(function (data) {
        $scope.result = data.data;
      });
    };
  });
